#pragma warning(disable: 4996)
#include "FileStream.h"
#include <stdio.h>
#define _CRT_SECURE_NO_WARNINGS
/*
The function tried to open a file
*/
FileStream::FileStream(char* filedst)
{
	_file = fopen(filedst, "w"); // the file we are going to write to is the opened of the file dst
	// we open him with w because every time the log ends we write to new file
	if (!_file) // if we cant open a file we will get null
		fprintf(stderr, "Error, cannot open file."); // print error 
}
FileStream::~FileStream()
{
	fclose(_file); // destractor must close the file
	_file = nullptr; // and clean the pointer
}
