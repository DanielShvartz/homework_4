#pragma once
#include <stdio.h>
class OutStream
{
protected:
	FILE * _file; // we add the field file
public:
	OutStream();
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE* _file)); // to run a function it must get a file
};

void endline(FILE* file); // endline gets the stdout
