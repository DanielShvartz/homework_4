#include "OutStream.h"
#include <stdio.h>

OutStream::OutStream()
{
	_file = stdout; // we say that it prints only to the screen and the inheritated classes
} // can change it.

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str) // we change in each function to fprintf
{
	fprintf(_file,"%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num) // but will get the file to print to stdout
{
	fprintf(_file,"%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE* _file)) // we cant print to the file only to screen
{
	pf(_file);
	return *this;
}

/*
OutStream must print to the screen we cant change it to print to the file
So, we change the file to be stdout so it prints only to the screen and if needed it prints to the file
*/
void endline(FILE* file)
{
	fprintf(file,"\n");
}
