#pragma once
#include "OutStream.h"
class OutStreamEncrypted:public OutStream
{
private:
	int _offset; // we must have offset
public:
	OutStreamEncrypted(int offset);
	~OutStreamEncrypted();
	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE* file));
	//  we must return refrence to OutStreamEncrypted because if we dont we cant go back to print as usual
	// we need a pointer function if not we cant print in 1 line.
};

