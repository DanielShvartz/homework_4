#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include <stdlib.h>
#include "Logger.h"
int main(int argc, char **argv)
{
	// "I am the Doctor and I'm 1500 years old"

	OutStream out = OutStream(); // we create an object of the outstream class
	//level 1
	// level 2, we must use all the function to print:
	out << endline;
	out << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	//level 3: print to file
	FileStream fileOut = FileStream("check.txt"); // we create a new txt file
	fileOut << "I am the Doctor and I'm " << 1500 << " years old" << endline; // and print to him
	// when we use fileout we just say print to this file and not out stream, then 
	//level 4: print encrypted
	OutStreamEncrypted outEncrypted = OutStreamEncrypted(3); // set offset
	outEncrypted << "I am the Doctor and I'm " << 1500 << " years old" << endline;
	Logger log = Logger();
	log << "I am the Doctor" << endline <<  "and I'm " << endline <<1500 << endline << " years old" << endline;
	Logger log2 = Logger();
	log2 << "I am the Doctor" << endline << "and I'm " << endline << 1500 << endline << " years old" << endline;
	system("pause");
	return 0;
}
