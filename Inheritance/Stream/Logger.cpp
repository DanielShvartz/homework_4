#include "Logger.h"
/*
The c'tor sets an outstream object to print, and sets a start line
*/
Logger::Logger()
{
	os = OutStream(); // we create an outstream 
	_startLine = true;
	// it must be new line.
}
Logger::~Logger()
{
	os.~OutStream(); // d'tor the os
}
/*
Function sets a new startline
*/
void Logger::setStartLine()
{
	static unsigned int logCounter = 0;
	if (_startLine) // if its the start of the line
	{
		logCounter++; // we add to the counter and print him
		os << endline << "LOG" << logCounter << ": "; // we move 1 line down as an end line and print log 
		_startLine = false; // change the start line because its not the start...
	}
}
Logger& operator<<(Logger& l, const char *msg)
{
	if (l._startLine) // we check if the start of the line
		l.setStartLine(); // if it is we start a new line
	l.os << msg; // print the msg
	return l; // return the logger so we could print later
}
Logger& operator<<(Logger& l, int num)
{
	if (l._startLine) // same but with number
		l.setStartLine();
	l.os << num;
	return l;
}
Logger& operator<<(Logger& l, void(*pf)(FILE* file))
{
	if (pf == endline) //if it is the end line we just stay its the start line
		l._startLine = true;
	else // if its not the end line 
	{
		if (l._startLine) // we check the start line
			l.setStartLine(); // if its the start line we start the new line
		l.os << pf; // call function and what he does, we must print log each time our function is doing something
	}
	return l;
		
}
